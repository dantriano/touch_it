import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'src/styles/style.dart';
import 'services/Routes.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          //ChangeNotifierProvider(builder: (context) => LocationsProvider())
        ],
        child: MaterialApp(
          supportedLocales: [
            const Locale('en', 'US'),
            const Locale('es', 'SP'),
          ],
          locale: Locale('en', 'US'),
          debugShowCheckedModeBanner: false,
          onGenerateRoute: Routes.routes(),
          theme: Style.appTheme(),
        ));
  }
}
