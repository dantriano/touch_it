library touch_itt;
import 'dart:io';

final baseURL = ((Platform.isAndroid) ? '10.0.2.2' : 'localhost');
const SCHEMA = "http";
const V_API = "api";
const PORT = {'socket':5000,'restapi':4000};
